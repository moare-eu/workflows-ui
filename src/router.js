import Vue from 'vue'
import VueRouter from 'vue-router'

import store from '@/store'

Vue.use(VueRouter)

const router = new VueRouter({
  mode: 'history',
  base: process.env.BASE_URL,
  routes: [
    {
      path: '/',
      name: 'dashboard',
      meta: { requiresAuth: true, menu: 'dashboard' },
      component: () =>
        import(/* webpackChunkName: "dashboard" */ './views/Dashboard.vue')
    },
    {
      path: '/login',
      name: 'login',
      meta: { layout: 'auth' },
      component: () =>
        import(/* webpackChunkName: "login" */ './views/Login.vue')
    },
    {
      path: '/tasks/',
      name: 'task-index',
      meta: { requiresAuth: true, menu: 'task' },
      component: () => import(/* webpackChunkName: "task-list" */ './views/TaskList.vue')
    },
    {
      path: '/tasks/:id',
      name: 'task-details',
      meta: { requiresAuth: true, menu: 'task' },
      component: () => import(/* webpackChunkName: "task-list" */ './views/TaskList.vue')
    },
    {
      path: '/history',
      name: 'history-index',
      meta: { requiresAuth: true, menu: 'history' },
      component: () => import(/* webpackChunkName: "history-list" */ './views/History.vue')
    },
    {
      path: '/history/:id',
      name: 'history-details',
      meta: { requiresAuth: true, menu: 'history' },
      component: () => import(/* webpackChunkName: "history-list" */ './views/History.vue')
    },
    {
      path: '/processes',
      name: 'process-index',
      meta: { requiresAuth: true, menu: 'process' },
      component: () => import(/* webpackChunkName: "process-list" */ './views/ProcessList.vue')
    },
    {
      path: '/processes/:id',
      name: 'process-details',
      meta: { requiresAuth: true, menu: 'process' },
      component: () => import(/* webpackChunkName: "process-list" */ './views/ProcessList.vue')
    },
    {
      path: '/processes/start',
      name: 'process-start',
      meta: { requiresAuth: true, menu: 'process-start' },
      component: () => import(/* webpackChunkName: "process-start" */ './views/ProcessStart.vue')
    },
    {
      path: '/user/create',
      name: 'user-create',
      meta: { requiresAuth: true, menu: 'user-create' },
      component: () => import(/* webpackChunkName: "user-create" */ './views/UserCreate.vue')
    },
    {
      path: '/user/password/change',
      name: 'user-password-change',
      meta: { requiresAuth: true, menu: 'user-password-change' },
      component: () => import(/* webpackChunkName: "user-password-change" */ './views/ChangePassword.vue')
    }
  ]
})

router.beforeEach((to, from, next) => {
  // Check before each request if authentication is required
  if (to.matched.some(record => record.meta.requiresAuth)) {
    if (!store.getters.isLoggedIn) {
      // console.log('Relogin needed')
      store.dispatch('relogin')
      if (!store.getters.isLoggedIn) {
        next({ name: 'login' })
      }
    }
  }
  next()
})

export default router
