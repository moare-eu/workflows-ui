<?php
header('Content-Type: application/json');

$htmlContent = file_get_contents("https://vsr.informatik.tu-chemnitz.de/edu/studentprojects/proposed/master/");

$DOM = new DOMDocument();

libxml_use_internal_errors(true);

$DOM->loadHTML($htmlContent);
//echo $DOM->saveHTML();
$Detail = $DOM->getElementsByTagName("h4");
//print_r ($Detail[0]);

foreach($Detail as $NodeHeader)
	{
		$aDataTableHeaderHTML[] = trim($NodeHeader->textContent);
	}

	$i = 0;
	$j = 0;
	foreach($Detail as $sNodeDetail)
	{
		$aDataTableDetailHTML[] = trim($sNodeDetail->textContent);
		$i = $i + 1;
		$j = $i % count($aDataTableHeaderHTML) == 0 ? $j + 1 : $j;
	}
//print_r($aDataTableDetailHTML); die();
echo json_encode($aDataTableDetailHTML);
?>
