import Metro from 'metro4'
import Vue from 'vue'

import App from './App.vue'
import AuthLayout from './layout/Auth.vue'
import MainLayout from './layout/Main.vue'
import router from './router'
import store from './store'

// import layout components
Vue.component('auth-layout', AuthLayout)
Vue.component('main-layout', MainLayout)

// register libraries
Vue.use(require('vue-moment'))

// import global libraries
window.Metro = Metro
window.$ = window.jquery = require('jquery')

Vue.config.productionTip = false

new Vue({
  router,
  store,
  render: h => h(App),
  mounted: function () {
    Metro.init()
  }
}).$mount('#app')
