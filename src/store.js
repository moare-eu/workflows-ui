import Vue from 'vue'
import Vuex from 'vuex'
import VueLodash from 'vue-lodash'

import CAS from '@/services/CamundaApiService'

const options = { name: 'lodash' }
Vue.use(VueLodash, options)

Vue.use(Vuex)

export default new Vuex.Store({
  state: {
    status: '',
    token: localStorage.getItem('auth_token') || '',
    user: JSON.parse(localStorage.getItem('user')),
    isLoggedIn: false
    // task: "",
    // auth: ""
  },
  mutations: {
    auth_request (state) {
      state.status = 'loading'
    },
    auth_success (state, payload) {
      state.status = 'success'
      state.isLoggedIn = true
      state.token = payload.token
      localStorage.setItem('auth_token', payload.token)
      state.user = payload.user
      localStorage.setItem('user', JSON.stringify(payload.user))
    },
    auth_error (state) {
      state.status = 'error'
      state.token = ''
      state.user = {}
      localStorage.removeItem('auth_token')
      localStorage.removeItem('user')
    },
    user_data (state, { user }) {
      state.user.first_name = user.first_name
      state.user.last_name = user.last_name
      localStorage.setItem('user', JSON.stringify(state.user))
    },
    logout (state) {
      state.status = ''
      state.token = ''
      state.user = {}
      state.isLoggedIn = false
      localStorage.removeItem('auth_token')
      localStorage.removeItem('user')
    }
    /* tasks(state, data) {
      state.task = data.task;
    } */
  },
  actions: {
    login ({ commit, dispatch }, user) {
      return new Promise((resolve, reject) => {
        commit('auth_request')
        let reqData = {
          username: user.email,
          password: user.password
        }
        let headers = {
          Authorization: 'Basic ' + btoa(user.email + ':' + user.password)
        }
        CAS().post('/identity/verify', reqData, { headers: headers })
          .then(res => {
            let token = btoa(reqData.username + ':' + reqData.password)
            let user = {
              id: res.data.authenticatedUser
            }
            commit('auth_success', { user, token })
            dispatch('getUserData', user['id'])
            resolve(res)
          })
          .catch(err => {
            commit('auth_error')
            reject(err)
          })
      })
    },

    getUserData ({ commit }, userId) {
      return new Promise((resolve, reject) => {
        CAS().get('/user/' + userId + '/profile')
          .then(res => {
            let user = {
              first_name: res.data.firstName,
              last_name: res.data.lastName
            }
            commit('user_data', { user })
            resolve(res)
          })
          .catch(err => {
            commit('auth_error')
            reject(err)
          })
      })
    },

    logout ({ commit }) {
      return new Promise((resolve/*, reject */) => {
        commit('logout')
        localStorage.removeItem('token')
        // delete axios.defaults.headers.common["Authorization"];
        resolve()
      })
    },

    relogin ({ commit, dispatch }) {
      let storedToken = localStorage.getItem('auth_token')
      let storedUser = JSON.parse(localStorage.getItem('user'))

      // if token and user_id stored in locale storage than set them in vue
      if (storedToken && storedUser) {
        commit('auth_success', { user: storedUser, token: storedToken })
        dispatch('getUserData', storedUser.id)
      } else {
        commit('auth_error')
      }
    }
  //   getTask({ commit }, user) {
  //     return new Promise((resolve, reject) => {
  //       const url = "https://camunda.moare.eu/engine-rest/history/task/count";
  //       const options = {
  //         method: "GET",
  //         headers: {
  //           Authorization: "Basic " + localStorage.getItem("auth"),
  //           "content-type": "application/json"
  //         },
  //         data: {
  //           taskAssignee: user.name
  //         },
  //         url
  //       };
  //       axios(options)
  //         .then(res => {
  //           console.log(res);
  //           const data = JSON.parse(JSON.stringify(res)).data;
  //           const task = {
  //             name: data.count
  //           };
  //           commit("tasks", { task });
  //           resolve(res);
  //         })
  //         .catch(err => {
  //           // localStorage.removeItem("token");
  //           console.log(err.response.message);
  //           reject(err);
  //         });
  //     });
  //   }
  },
  getters: {
    getUserData: state => state.user,
    isLoggedIn: state => state.isLoggedIn,
    // authStatus: state => state.status
    token: state => state.token
  }
})
