import axios from 'axios'
import store from '@/store'

export default () => {
  let headers = {}

  if (store.getters.token) {
    headers['Authorization'] = 'Basic ' + store.getters.token
  }
  return axios.create({
    baseURL: process.env.VUE_APP_HOST_WORKFLOWS_SERVER,
    headers: headers
  })
}
