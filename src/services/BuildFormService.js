import CAS from '@/services/CamundaApiService'

export default {

  checkForConstraints (data) {
    // helper method
    var cons = {}
    if ($(data).find('input').first().attr('min') !== undefined) {
      cons.min = $(data).find('input').first().attr('min')
      cons.required = 'required'
    }
    if ($(data).find('input').first().attr('max') !== undefined) {
      cons.max = $(data).find('input').first().attr('max')
      cons.required = 'required'
    }
    if ($(data).find('input').first().attr('minlength') !== undefined) {
      cons.minlength = $(data).find('input').first().attr('minlength')
      cons.required = 'required'
    }
    if ($(data).find('input').first().attr('maxlength') !== undefined) {
      cons.maxlength = $(data).find('input').first().attr('maxlength')
      cons.required = 'required'
    }
    if ($(data).find('input').first().attr('required') !== undefined) {
      cons.required = $(data).find('input').first().attr('required')
    }
    if ($(data).find('input').first().attr('readonly') !== undefined) {
      cons.readonly = $(data).find('input').first().attr('readonly')
    }
    return cons
  },

  /**
   * Generate from rendered form data from camunda.
   *
   * @param {*} data
   */
  async buildForm (data) {
    // every input field is wrapped in div with class "form-group"
    var html = $(data)
    // store all divs with input elements in 1 array
    var formGroups = $(html).find('div.form-group').toArray()
    var formElements = []
    var self = this
    // generate array with objects for every input field
    formGroups.forEach(function (formGroup) {
      var inputFieldJSON = self.generateJSON(formGroup) // returns object
      formElements.push(inputFieldJSON) // add that object to array of form elements
    })
    // generate the basic form element --> every input field will be appended to this
    var formDiv = $('<div></div>')
    // generate the HTML for every element (object) in that array
    for (const element of formElements) {
      var result
      switch (element.type) {
        case 'String': case 'Long':
          result = await self.generateTextField(element)
          $(formDiv).append(result)
          break
        case 'Boolean':
          result = self.generateCheckbox(element)
          // console.log(result)
          $(formDiv).append(result)
          break
        case 'Enum':
          result = self.generateEnumList(element)
          // console.log(result)
          $(formDiv).append(result)
          break
        case 'Date':
          result = self.generateDatePicker(element)
          // console.log(result)
          $(formDiv).append(result)
          break
        default:
          break
      }
    }
    // console.log('done')
    return $(formDiv).html()
  },

  generateCheckbox (element) {
    // Create something like: <input type="checkbox" data-role="checkbox" data-caption="Checkbox">
    var outerDiv = $('<div></div>').addClass('form-group')
    /* LABEL */
    var input = $('<input/>').addClass('form-control').attr('type', 'checkbox')
    input = $(input).attr('name', element.name)
    input = $(input).attr('data-role', 'checkbox')
    input = $(input).attr('data-caption', element.label)
    var inputGroup = $(outerDiv).append(input)
    return inputGroup
  },

  generateDatePicker (element) {
    var outerDiv = $('<div></div>').addClass('form-group')
    /* LABEL */
    var label = $('<label></label>').attr('for', element.name).text(element.label)
    var input = $('<input/>').addClass('form-control').attr('name', element.name)
    input = $(input).attr('data-role', 'datepicker')
    for (var key in element.cons) {
      if (element.cons.hasOwnProperty(key)) {
        if (key === 'readonly') {
          $(input).prop('readonly', true)
          $(input).prop('placeholder', element.value)
        }
        var previousVal = ''
        if ($(input).attr('data-validate') !== undefined) previousVal = $(input).attr('data-validate')
        var newVal = key + '=' + element.cons[key]
        if (key === element.cons[key]) newVal = key // e.g. instead of readonly=readonly --> readonly
        input = $(input).attr('data-validate', (previousVal + ' ' + newVal).trim())
      }
    }
    // display default value if there is one
    if (element.value !== undefined) $(input).attr('data-value', element.value)
    var inputGroup = $(outerDiv).append(label, input)
    return inputGroup
  },

  generateEnumList (element) {
    var outerDiv = $('<div></div>').addClass('form-group')
    /* LABEL */
    var label = $('<label></label>').attr('for', element.name).text(element.label)
    /* SELECT */
    var select = $('<select />').attr('data-role', 'select')
    element.options.forEach(function (el) {
      // each el has attributes "name" (=> attribute "value") and "value" (=> content of HTML element)
      var option = $('<option></option>').attr('value', el.name)
      $(option).text(el.value)
      $(select).append(option)
    })
    var inputGroup = $(outerDiv).append(label, select)
    return inputGroup
  },

  generateJSON (data) {
    // special case: type is enum (= select list)
    var label, name, type, value
    if ($(data).find('select').first().length === 1) {
      type = 'Enum'
      label = $(data).find('label').first().text().trim()
      name = $(data).find('select').first().attr('name')
      value = $(data).find('select').first().attr('value')
      var options = []
      var optionsList = $(data).find('option').toArray()
      optionsList.forEach(function (option) {
        var optionName = $(option).attr('value')
        var optionValue = $(option).text().trim()
        var optionObj = { name: optionName, value: optionValue }
        // generate array with objects, each object contains 1 option
        options.push(optionObj)
      })
      var optionsObj = { label, type, name, value, options }
      return optionsObj
    }
    // normal case: type is string, number or boolean
    label = $(data).find('label').first().text().trim()
    type = $(data).find('input').first().attr('cam-variable-type')
    // special case: date --> datepicker-popup="dd/MM/yyyy"
    if ($(data).find('input').first().attr('datepicker-popup') === 'dd/MM/yyyy') {
      type = 'Date'
    }
    // var valueInfo = {};
    name = $(data).find('input').first().attr('name')
    value = $(data).find('input').first().attr('value')
    // check for constraints
    var cons = this.checkForConstraints(data)
    var obj = { label, type, name, value, cons }
    return obj
  },

  async generateTextField (element) {
    var outerDiv = $('<div></div>').addClass('form-group')
    /* LABEL */
    var label = $('<label></label>').attr('for', element.name).text(element.label)
    var input
    if (element.type === 'String' && element.name.startsWith('long_')) {
      input = $('<textarea />').addClass('form-control').attr('name', element.name)
    } else {
      input = $('<input/>').addClass('form-control').attr('name', element.name)
    }
    var warning
    /* INPUT FIELD */
    // Note: Maybe use Textbox if input name starts with "text_long" or so
    if (element.type === 'String') {
      var id = element.name
      if (element.type === 'String' && id.startsWith('long_')) {
        input = $(input).attr('data-role', 'textarea')
        input = $(input).attr('data-auto-size', 'true')
        input = $(input).attr('data-max-height', '50')
      } else {
        input = $(input).attr('type', 'text')
        input = $(input).attr('data-role', 'input')
      }
      var idParts = id.split('_')
      if (idParts.length > 3 && idParts[0] === 'assign' && idParts[1] === 'user') {
        input = await this.autocompleteUserAssignments(input, idParts[2])
      } else if (id.startsWith('assign_group_')) {
        input = await this.autocompleteGroupAssignments(input, idParts[2], element)
      }
      // input = $(input).attr('data-validate', 'required minlength=6');
    } else if (element.type === 'Long') {
      input = $(input).attr('type', 'number')
      input = $(input).attr('data-role', 'input')
    }
    // add constraints
    input = this.addConstraints(element, input)
    warning = $('<span></span>').addClass('invalid_feedback').text('Invalid input.')
    // display default value if there is one
    if (element.value !== undefined) {
      var val
      if (id.startsWith('date_')) {
        val = element.value.substr(0, 10)
      } else {
        val = element.value
      }
      $(input).attr('value', val)
    }
    var inputGroup = $(outerDiv).append(label, input, warning)
    return inputGroup
  },

  addConstraints: function (element, input) {
    for (var key in element.cons) {
      if (element.cons.hasOwnProperty(key)) {
        if (key === 'readonly') {
          $(input).prop('readonly', true)
          $(input).prop('placeholder', element.value)
        }
        var previousVal = ''
        if ($(input).attr('data-validate') !== undefined) previousVal = $(input).attr('data-validate')
        var newVal = key + '=' + element.cons[key]
        if (key === element.cons[key]) newVal = key // e.g. instead of readonly=readonly --> readonly
        input = $(input).attr('data-validate', (previousVal + ' ' + newVal).trim())
      }
    }
    return input
  },
  async autocompleteUserAssignments (input, groupname) {
    var inputNew = input
    var res = await CAS().get('user/?memberOfGroup=' + groupname)
    var users = []
    for (const user of res.data) {
      users.push(user.id)
    }
    inputNew = $(input).attr('data-autocomplete', users)
    return inputNew
  },

  async autocompleteGroupAssignments (input) {
    var inputNew = input
    var res = await CAS.get('group')
    var groups = []
    for (const group of res.data) {
      groups.push(group.id)
    }
    inputNew = $(input).attr('data-autocomplete', groups)
    return inputNew
  }
}
