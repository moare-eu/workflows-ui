# moare-ui-user

## Project setup
```
npm install
```

### Compiles and hot-reloads for development
```
npm run serve
```

### Compiles and minifies for production
```
npm run build
```

### Lints and fixes files
```
npm run lint
```
### Activate Flexible Tasks

1. Upload the 'FlexAct.v1.cmmn' to Camunda Server.
2. Create a human task in corresponding bpmn file where id starts with 'flex_act_assign'. Now flexible tasks could be created inside this task.
3. This task should follow by a call activity.
4. Call Activity Type schould be 'CMMN' with Case Ref 'FlexAct1' and Tenant Id 'FlexAct1'.
5. Under Variables in this Call Activity add a In and Out Mapping from Type 'all'.

Now Flexible Activities could be used inside MOARE WORKFLOWS.

### Start development

1. Create '.env' file in root directory
2. Edit file and set VUE_APP_HOST_WORKFLOWS_SERVER. For example: VUE_APP_HOST_WORKFLOWS_SERVER=http://localhost:8080/engine-rest
(3.) Alternative the variable could be set as system variable